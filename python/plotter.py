import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def main():
    xGrid = np.loadtxt('../build/code/xGrid.txt', usecols=range(1,0))
    yGrid = np.loadtxt('../build/code/yGrid.txt', usecols=range(1,0))
    Nx = len(xGrid)
    Ny = len(yGrid)

    flux  = np.loadtxt('../build/code/flux.txt',  usecols=range(Ny,Nx))

    X, Y = np.meshgrid(xGrid, yGrid)

    fig = plt.figure(figsize=(6,5))
    left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
    ax = fig.add_axes([left, bottom, width, height]) 

    cp = plt.contourf(X, Y, flux, cmap=plt.cm.plasma)
    plt.colorbar(cp)
    
    ax.set_xlabel('x (cm)')
    ax.set_ylabel('y (cm)')
    ax.set_title('$\Phi(x,y)$ Contour Plot')

    fig2 = plt.figure(2)
    ax2 = fig2.gca(projection='3d')

    surf = ax2.plot_surface(X, Y, flux, cmap=cm.plasma,
                            linewidth=0, antialiased=False)
    ax2.set_xlabel('x (cm)')
    ax2.set_ylabel('y (cm)')
    ax2.set_zlabel('$\Phi(x,y)$')
    
    plt.show()

if __name__ == '__main__': main()
