template<typename P>
class FiniteElement {
  P problem;
  double xMin, xMax, yMin, yMax;
  std::vector<double> xGrid, yGrid;

  std::vector<double> buildGrid(double min, double max, int N){
    std::vector<double> grid(N+1, 0.0);
    for(int i = 0; i < N+1; ++i){
      grid[i] = min + static_cast<double>(i)/(N)*(max - min);
    }
    return grid;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> M  = { {4.0, 2.0, 1.0, 2.0},
							  {2.0, 4.0, 2.0, 1.0},
							  {1.0, 2.0, 4.0, 2.0},
							  {2.0, 1.0, 2.0, 4.0} };

  blaze::DynamicMatrix<double, blaze::columnMajor> Lx = { { 2.0,  2.0,  1.0,  1.0},
							  {-2.0, -2.0, -1.0, -1.0},
							  {-1.0, -1.0, -2.0, -2.0},
							  { 1.0,  1.0,  2.0,  2.0} };

  blaze::DynamicMatrix<double, blaze::columnMajor> Ly = { { 2.0,  1.0,  1.0,  2.0},
							  { 1.0,  2.0,  2.0,  1.0},
							  {-1.0, -2.0, -2.0, -1.0},
							  {-2.0, -1.0, -1.0, -2.0} };

  /* x-plus */
  blaze::DynamicMatrix<double, blaze::columnMajor> Axp = { {0.0, 0.0, 0.0, 0.0},
							   {0.0, 2.0, 1.0, 0.0},
							   {0.0, 1.0, 2.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0} };

  /* x-minus */
  blaze::DynamicMatrix<double, blaze::columnMajor> Axm = { {2.0, 0.0, 0.0, 1.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {1.0, 0.0, 0.0, 2.0} };

  /* y-plus */
  blaze::DynamicMatrix<double, blaze::columnMajor> Ayp = { {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 2.0, 1.0},
							   {0.0, 0.0, 1.0, 2.0} };

  /* y-minus */
  blaze::DynamicMatrix<double, blaze::columnMajor> Aym = { {2.0, 1.0, 0.0, 0.0},
							   {1.0, 2.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0} };

  /* mu > 0 */
  blaze::DynamicMatrix<double, blaze::columnMajor> Vxp = { {0.0, 2.0, 1.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {0.0, 1.0, 2.0, 0.0} };

  /* mu < 0 */
  blaze::DynamicMatrix<double, blaze::columnMajor> Vxm = { {0.0, 0.0, 0.0, 0.0},
							   {2.0, 0.0, 0.0, 1.0},
							   {1.0, 0.0, 0.0, 2.0},
							   {0.0, 0.0, 0.0, 0.0} };

  /* eta > 0 */
  blaze::DynamicMatrix<double, blaze::columnMajor> Vyp = { {0.0, 0.0, 1.0, 2.0},
							   {0.0, 0.0, 2.0, 1.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0} };

  /* eta < 0 */
  blaze::DynamicMatrix<double, blaze::columnMajor> Vym = { {0.0, 0.0, 0.0, 0.0},
							   {0.0, 0.0, 0.0, 0.0},
							   {1.0, 2.0, 0.0, 0.0},
							   {2.0, 1.0, 0.0, 0.0} };
  
public:
  int Nx, Ny;

  FiniteElement(P& problem) : problem(problem)
  {
    this->xMin = problem.xMin;
    this->xMax = problem.xMax;
    this->yMin = problem.yMin;
    this->yMax = problem.yMax;
    this->Nx = problem.Nx;
    this->Ny = problem.Ny;
    this->xGrid = this->buildGrid(this->xMin, this->xMax, this->Nx);
    this->yGrid = this->buildGrid(this->yMin, this->yMax, this->Ny);
  }

  std::vector<double> getXgrid()
  {
    return this->xGrid;
  }

  std::vector<double> getYgrid()
  {
    return this->yGrid;
  }
  
  blaze::DynamicMatrix<double, blaze::columnMajor> getM(int i, int j)
  {
    double dx = this->xGrid[i+1] - this->xGrid[i];
    double dy = this->yGrid[j+1] - this->yGrid[j];
    return (dx*dy/36.0)*this->M;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getLx(int j)
  {
    double dy = this->yGrid[j+1] - this->yGrid[j];
    return (dy/12.0)*this->Lx;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getLy(int i)
  {
    double dx = this->xGrid[i+1] - this->xGrid[i];
    return (dx/12.0)*this->Ly;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getAxp(int j)
  {
    double dy = this->yGrid[j+1] - this->yGrid[j];
    return (dy/6.0)*this->Axp;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getAxm(int j)
  {
    double dy = this->yGrid[j+1] - this->yGrid[j];
    return (dy/6.0)*this->Axm;
  }
  
  blaze::DynamicMatrix<double, blaze::columnMajor> getAyp(int i)
  {
    double dx = this->xGrid[i+1] - this->xGrid[i];
    return (dx/6.0)*this->Ayp;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getAym(int i)
  {
    double dx = this->xGrid[i+1] - this->xGrid[i];
    return (dx/6.0)*this->Aym;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getVxp(int j)
  {
    double dy = this->yGrid[j+1] - this->yGrid[j];
    return (dy/6.0)*this->Vxp;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getVxm(int j)
  {
    double dy = this->yGrid[j+1] - this->yGrid[j];
    return (dy/6.0)*this->Vxm;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getVyp(int i)
  {
    double dx = this->xGrid[i+1] - this->xGrid[i];
    return (dx/6.0)*this->Vyp;
  }

  blaze::DynamicMatrix<double, blaze::columnMajor> getVym(int i)
  {
    double dx = this->xGrid[i+1] - this->xGrid[i];
    return (dx/6.0)*this->Vym;
  }
};
