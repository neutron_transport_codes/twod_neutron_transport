#define CATCH_CONFIG_MAIN
#include "2D_Neutron_Transport.hpp"
#include <catch2/catch.hpp>

using namespace NTR_2D::Methods;

SCENARIO("Test NumericalSn::Source") {
  double XMIN = 0.0;
  double XMAX = 2.0;
  int Nx = 4;
  
  double YMIN = 0.0;
  double YMAX = 2.0;
  int Ny = 4;
  
  double sigT = 2.00;
  double c    = 0.90;

  /* Left Boundary */
  auto f = [](auto&& y, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + 0.0*y + 0.0; };
  /* Right Boundary */
  auto g = [](auto&& y, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + 0.0*y + 0.0; };
  /* Down Boundary */
  auto h = [](auto&& x, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + 0.0*x + 0.0; };
  /* Up  Boundary */
  auto i = [](auto&& x, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + 0.0*x + 0.0; };
  
  NTR_2D::Tools::Quad::Square<2> quad;
  NTR_2D::Tools::Quad::TwoDimensionalLegendre<2, 2> quad_2d;

  WHEN(" using a constant source ") {
    auto Q = [](auto&& x, auto&& y, auto&& mu, auto&& eta) { return 0.0*x + 0.0*y + 0.0*mu + 0.0*eta + 1.0/(4.0*PI); };
    
    NTR_2D::Methods::NumericalSn::Problem<decltype(Q), decltype(f), decltype(g), decltype(h), decltype(i)>
      prob(Q, f, g, h, i, XMIN, XMAX, YMIN, YMAX, Nx, Ny, sigT, c);
    
    NTR_2D::Methods::NumericalSn::FiniteElement<decltype(prob)>          finiteElement(prob);
    NTR_2D::Methods::NumericalSn::Source<decltype(quad), decltype(prob)> source(quad, prob);
    source.calcSource(finiteElement, quad_2d);
    
    std::vector<double> xGrid = finiteElement.getXgrid();
    std::vector<double> yGrid = finiteElement.getYgrid();
    
    auto actual = [](double l, double r, double d, double u){ return (r - l)*(u - d)/(16.0*PI); };
    
    for(int i = 0; i < Nx; ++i) {
      double l = xGrid[i];
      double r = xGrid[i+1];
      
      for(int j = 0; j < Ny; ++j) {
	double d = yGrid[j];
	double u = yGrid[j+1];

	auto vec = source.from(i, j, 1);
	double res = actual(l,r,d,u);

	REQUIRE( vec[0] == Approx( res ).epsilon( 1E-14) );
	REQUIRE( vec[1] == Approx( res ).epsilon( 1E-14) );
	REQUIRE( vec[2] == Approx( res ).epsilon( 1E-14) );
	REQUIRE( vec[3] == Approx( res ).epsilon( 1E-14) );
      }
    }
  }
}
