#include "2D_Neutron_Transport.hpp"
#include <catch2/catch.hpp>

using namespace NTR_2D::Methods;

int NX = 4;
double xMin = 0.0;
double xMax = 4.0;

int NY = 4;
double yMin = 0.0;
double yMax = 4.0;

double sigT = 2.00;
double c    = 0.90;

auto Q = [](auto&& x, auto&& y, auto&& mu, auto&& eta) { return 0.0*x + 0.0*y + 0.0*mu + 0.0*eta + 1.0/(4.0*PI); };
auto f = [](auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta; };
auto g = [](auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta; };
auto h = [](auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta; };
auto i = [](auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta; };
  
NTR_2D::Methods::NumericalSn::Problem<decltype(Q), decltype(f), decltype(g), decltype(h), decltype(i)>
prob(Q, f, g, h, i, xMin, xMax, yMin, yMax, NX, NY, sigT, c);

NTR_2D::Methods::NumericalSn::FiniteElement<decltype(prob)>  fE(prob);

SCENARIO("Test NumericalSn::FinitElement") {

  WHEN(" constructing the x and y spatial grids, ") {
    std::vector<double> grid{0.0, 1.0, 2.0, 3.0, 4.0};
    auto xGrid = fE.getXgrid();
    auto yGrid = fE.getYgrid();
    for(int i = 0; i < 5; ++i) {
      REQUIRE( xGrid[i] == grid[i] );
      REQUIRE( yGrid[i] == grid[i] );
    }
  }

  WHEN(" constructing a mass matrix, ") {
    int i = 2;
    int j = 2;
    blaze::DynamicMatrix<double, blaze::columnMajor> M  = { {4.0, 2.0, 1.0, 2.0},
							    {2.0, 4.0, 2.0, 1.0},
							    {1.0, 2.0, 4.0, 2.0},
							    {2.0, 1.0, 2.0, 4.0} };
    auto fE_M = fE.getM(i,j);
    for(int i = 0; i < 4; ++i) {
      for(int j = 0; j < 4; ++j) {
	REQUIRE( (1.0/36.0)*M(i,j) == fE_M(i,j) );
      }
    }
  }

  WHEN(" constructing Lx and Ly, ") {
    int i = 2;
    int j = 2;
    blaze::DynamicMatrix<double, blaze::columnMajor> Lx = { { 2.0,  2.0,  1.0,  1.0},
							    {-2.0, -2.0, -1.0, -1.0},
							    {-1.0, -1.0, -2.0, -2.0},
							    { 1.0,  1.0,  2.0,  2.0} };

    blaze::DynamicMatrix<double, blaze::columnMajor> Ly = { { 2.0,  1.0,  1.0,  2.0},
							    { 1.0,  2.0,  2.0,  1.0},
							    {-1.0, -2.0, -2.0, -1.0},
							    {-2.0, -1.0, -1.0, -2.0} };

    auto fE_Lx = fE.getLx(j);
    auto fE_Ly = fE.getLy(i);
    for(int i = 0; i < 4; ++i) {
      for(int j = 0; j < 4; ++j) {
	REQUIRE( (1.0/12.0)*Lx(i,j) == fE_Lx(i,j) );
	REQUIRE( (1.0/12.0)*Ly(i,j) == fE_Ly(i,j) );
      }
    }
  }

  WHEN(" constructing x-spatial upwinding, ") {
    int j = 2;

    /* x-plus */
    blaze::DynamicMatrix<double, blaze::columnMajor> Axp = { {0.0, 0.0, 0.0, 0.0},
							     {0.0, 2.0, 1.0, 0.0},
							     {0.0, 1.0, 2.0, 0.0},
							     {0.0, 0.0, 0.0, 0.0} };

    /* x-minus */
    blaze::DynamicMatrix<double, blaze::columnMajor> Axm = { {2.0, 0.0, 0.0, 1.0},
							     {0.0, 0.0, 0.0, 0.0},
							     {0.0, 0.0, 0.0, 0.0},
							     {1.0, 0.0, 0.0, 2.0} };

    blaze::DynamicVector<double, blaze::columnVector> vec{1.0, 1.0, 1.0, 1.0};
    blaze::DynamicVector<double, blaze::columnVector> xpl{3.0/6.0, 0.0, 0.0, 3.0/6.0};
    blaze::DynamicVector<double, blaze::columnVector> xmi{0.0, 3.0/6.0, 3.0/6.0, 0.0};

    auto fE_Axp = fE.getAxp(j);
    auto fE_Axm = fE.getAxm(j);

    auto fE_Vxp = fE.getVxp(j);
    auto fE_Vxm = fE.getVxm(j);

    auto fE_xpl = fE_Vxp*vec;
    auto fE_xmi = fE_Vxm*vec;

    for(int i = 0; i < 4; ++i) {
      for(int j = 0; j < 4; ++j) {
	REQUIRE( (1.0/6.0)*Axp(i,j) == fE_Axp(i,j) );
	REQUIRE( (1.0/6.0)*Axm(i,j) == fE_Axm(i,j) );
      }
    }

    for(int i = 0; i < 4; ++i) {
      REQUIRE( xpl[i] == fE_xpl[i] );
      REQUIRE( xmi[i] == fE_xmi[i] );
    }
  }

  WHEN(" constructing y-spatial upwinding, ") {
    int i = 2;

    /* y-plus */
    blaze::DynamicMatrix<double, blaze::columnMajor> Ayp = { {0.0, 0.0, 0.0, 0.0},
							     {0.0, 0.0, 0.0, 0.0},
							     {0.0, 0.0, 2.0, 1.0},
							     {0.0, 0.0, 1.0, 2.0} };

    /* y-minus */
    blaze::DynamicMatrix<double, blaze::columnMajor> Aym = { {2.0, 1.0, 0.0, 0.0},
							     {1.0, 2.0, 0.0, 0.0},
							     {0.0, 0.0, 0.0, 0.0},
							     {0.0, 0.0, 0.0, 0.0} };

    blaze::DynamicVector<double, blaze::columnVector> vec{1.0, 1.0, 1.0, 1.0};
    blaze::DynamicVector<double, blaze::columnVector> ypl{3.0/6.0, 3.0/6.0, 0.0, 0.0};
    blaze::DynamicVector<double, blaze::columnVector> ymi{0.0, 0.0, 3.0/6.0, 3.0/6.0};

    auto fE_Ayp = fE.getAyp(i);
    auto fE_Aym = fE.getAym(i);

    auto fE_Vyp = fE.getVyp(i);
    auto fE_Vym = fE.getVym(i);

    auto fE_ypl = fE_Vyp*vec;
    auto fE_ymi = fE_Vym*vec;

    for(int i = 0; i < 4; ++i) {
      for(int j = 0; j < 4; ++j) {
	REQUIRE( (1.0/6.0)*Ayp(i,j) == fE_Ayp(i,j) );
	REQUIRE( (1.0/6.0)*Aym(i,j) == fE_Aym(i,j) );
      }
    }

    for(int i = 0; i < 4; ++i) {
      REQUIRE( ypl[i] == fE_ypl[i] );
      REQUIRE( ymi[i] == fE_ymi[i] );
    }
  }
}
    
    
