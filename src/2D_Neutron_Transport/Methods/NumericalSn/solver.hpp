template<typename P, typename Q, typename S, typename F>
auto solver(P&& problem, Q&& quad, S&& source, F&& finiteElement, double tolerance)
{
  std::cout << "#######################################\n";
  std::cout << "############# 2D Sn Begin #############\n";
  std::cout << "#######################################\n";

  int Nx = problem.Nx;
  int Ny = problem.Ny;
  double sigT = problem.sigT;
  double c    = problem.c;
  
  auto quadrature = quad.getQuadrature();
  int M = quadrature.size();

  int N = 4;
  const std::unique_ptr<blaze::blas_int_t[]> ipiv( new blaze::blas_int_t[N] );

  /* Initialize Flux Grids */
  std::unique_ptr<double[]> Ns1 = std::make_unique<double[]>(Ny * Nx * 4);
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right>
    scalarFlux1(Ns1.get(), Ny, Nx, 4);

  /* Initialize Matrix and Vector Needed for Sweeps */
  blaze::DynamicMatrix<double, blaze::columnMajor> Amatrix(4, 4, 0.0);
  blaze::DynamicVector<double, blaze::columnVector> b(4, 0.0);
  blaze::DynamicVector<double, blaze::columnVector> scalar(4, 0.0);

  /* Initialize Matrices for Tracking Leackage */
  std::vector<std::vector<double>> leak_l_1(Ny, std::vector<double>(M, 0.0));
  std::vector<std::vector<double>> leak_l_4(Ny, std::vector<double>(M, 0.0));
  
  std::vector<std::vector<double>> leak_r_2(Ny, std::vector<double>(M, 0.0));
  std::vector<std::vector<double>> leak_r_3(Ny, std::vector<double>(M, 0.0));

  std::vector<std::vector<double>> leak_d_1(Nx, std::vector<double>(M, 0.0));
  std::vector<std::vector<double>> leak_d_2(Nx, std::vector<double>(M, 0.0));

  std::vector<std::vector<double>> leak_u_3(Nx, std::vector<double>(M, 0.0));
  std::vector<std::vector<double>> leak_u_4(Nx, std::vector<double>(M, 0.0));

  blaze::DynamicVector<double, blaze::columnVector> y_upwind(4, 0.0);
  blaze::DynamicVector<double, blaze::columnVector> x_upwind(4, 0.0);

  std::unique_ptr<double[]> Na1 = std::make_unique<double[]>(Ny * Nx * 4);
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right>
    angularFlux(Na1.get(), Ny, Nx, 4);
  
  /* Begin Source Iteration */
  double error1 = 1.0;
  double error2 = 1.0;
  double rho = 0.0;
  double tolerance_m = 0.0;
  int it = 0;
  bool condition = true;
  do {
    std::unique_ptr<double[]> Ns2 = std::make_unique<double[]>(Ny * Nx * 4);
    
    stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right>
      scalarFlux2(Ns2.get(), Ny, Nx, 4);

    for(int m = 0; m < M; ++m) {
      double mu  = std::get<0>(quadrature[m]);
      double eta = std::get<1>(quadrature[m]);
      double wei = std::get<2>(quadrature[m]);

      if(mu > 0 && eta > 0) {
	for(int j = 0; j < Ny; ++j) {
	  for(int i = 0; i < Nx; ++i) {
	    b.reset();
	    /* y-upwind */
	    y_upwind.reset();
	    if(j == 0) {
	      y_upwind = source.bottom(i,m);
	      b += eta*source.bottom(i,m);
	    } else {
	      y_upwind[0] = angularFlux(j-1, i, 0);
	      y_upwind[1] = angularFlux(j-1, i, 1);
	      y_upwind[2] = angularFlux(j-1, i, 2);
	      y_upwind[3] = angularFlux(j-1, i, 3);
	    }
	    b += eta*finiteElement.getVyp(i)*y_upwind;
	    
	    /* x-upwind */
	    x_upwind.reset();
	    if(i == 0) {
	      x_upwind = source.left(j,m);
	      b += mu*source.left(j,m);
	    } else {
	      x_upwind[0] = angularFlux(j, i-1, 0);
	      x_upwind[1] = angularFlux(j, i-1, 1);
	      x_upwind[2] = angularFlux(j, i-1, 2);
	      x_upwind[3] = angularFlux(j, i-1, 3);
	    }
	    b += mu*finiteElement.getVxp(j)*x_upwind;

	    /* Get Scalar Flux */
	    scalar[0] = scalarFlux1(j,i,0);
	    scalar[1] = scalarFlux1(j,i,1);
	    scalar[2] = scalarFlux1(j,i,2);
	    scalar[3] = scalarFlux1(j,i,3);

	    /* Build A and b */
	    Amatrix  = mu*finiteElement.getLx(j) + eta*finiteElement.getLy(i) + sigT*finiteElement.getM(i,j);
	    
	    /* Handle Upwinding */
	    Amatrix += mu*finiteElement.getAxp(j) + eta*finiteElement.getAyp(i);

	    /* Source Vector */
	    b += (c*sigT/(2*PI))*finiteElement.getM(i,j)*scalar + source.from(i,j,m);

	    /* Solve system */
	    blaze::gesv( Amatrix, b, ipiv.get() );
	    
	    /* Update Psi */
	    angularFlux(j,i,0) = b[0];
	    angularFlux(j,i,1) = b[1];
	    angularFlux(j,i,2) = b[2];
	    angularFlux(j,i,3) = b[3];
	  
	    /* Update Phi */
	    scalarFlux2(j,i,0) += wei*b[0];
	    scalarFlux2(j,i,1) += wei*b[1];
	    scalarFlux2(j,i,2) += wei*b[2];
	    scalarFlux2(j,i,3) += wei*b[3];

	    if(i == Nx-1) {
	      leak_r_2[j][m] = b[1];
	      leak_r_3[j][m] = b[2];
	    }
	    if(j == Ny-1) {
	      leak_u_3[i][m] = b[2];
	      leak_u_4[i][m] = b[3];
	    }
	  }
	}

      } else if(mu > 0 && eta < 0) {
	for(int j = Ny-1; j>=0; --j) {
	  for(int i = 0; i < Nx; ++i) {
	    b.reset();
	    /* y-upwind */
	    y_upwind.reset();
	    if(j == Ny-1) {
	      y_upwind = source.top(i,m);
	      b -= eta*source.top(i,m);
	    } else {
	      y_upwind[0] = angularFlux(j+1, i, 0);
	      y_upwind[1] = angularFlux(j+1, i, 1);
	      y_upwind[2] = angularFlux(j+1, i, 2);
	      y_upwind[3] = angularFlux(j+1, i, 3);
	    }
	    b -= eta*finiteElement.getVym(i)*y_upwind;
	  
	    /* x-upwind */
	    x_upwind.reset();
	    if(i == 0) {
	      x_upwind = source.left(j,m);
	      b += mu*source.left(j,m);
	    } else {
	      x_upwind[0] = angularFlux(j, i-1, 0);
	      x_upwind[1] = angularFlux(j, i-1, 1);
	      x_upwind[2] = angularFlux(j, i-1, 2);
	      x_upwind[3] = angularFlux(j, i-1, 3);
	    }
	    b += mu*finiteElement.getVxp(j)*x_upwind;

	    /* Get Scalar Flux */
	    scalar[0] = scalarFlux1(j,i,0);
	    scalar[1] = scalarFlux1(j,i,1);
	    scalar[2] = scalarFlux1(j,i,2);
	    scalar[3] = scalarFlux1(j,i,3);

	    /* Build A and b */
	    Amatrix  = mu*finiteElement.getLx(j) + eta*finiteElement.getLy(i) + sigT*finiteElement.getM(i,j);

	    /* Handle Upwinding */
	    Amatrix += mu*finiteElement.getAxp(j) - eta*finiteElement.getAym(i);

	    /* Source Vector */
	    b += (c*sigT/(2*PI))*finiteElement.getM(i,j)*scalar + source.from(i,j,m);

	    /* Solve system */
	    blaze::gesv( Amatrix, b, ipiv.get() );

	    /* Update Psi */
	    angularFlux(j,i,0) = b[0];
	    angularFlux(j,i,1) = b[1];
	    angularFlux(j,i,2) = b[2];
	    angularFlux(j,i,3) = b[3];
	  
	    /* Update Phi */
	    scalarFlux2(j,i,0) += wei*b[0];
	    scalarFlux2(j,i,1) += wei*b[1];
	    scalarFlux2(j,i,2) += wei*b[2];
	    scalarFlux2(j,i,3) += wei*b[3];

	    if(i == Nx-1) {
	      leak_r_2[j][m] = b[1];
	      leak_r_3[j][m] = b[2];
	    }
	    if(j == 0) {
	      leak_d_1[i][m] = b[0];
	      leak_d_2[i][m] = b[1];
	    }

	  }
	}

      } else if(mu < 0 && eta > 0) {
	for(int j = 0; j < Ny; ++j) {
	  for(int i = Nx-1; i>=0; --i) {
	    b.reset();
	    /* y-upwind */
	    y_upwind.reset();
	    if(j == 0) {
	      y_upwind = source.bottom(i,m);
	      b += eta*source.bottom(i,m);
	    } else {
	      y_upwind[0] = angularFlux(j-1, i, 0);
	      y_upwind[1] = angularFlux(j-1, i, 1);
	      y_upwind[2] = angularFlux(j-1, i, 2);
	      y_upwind[3] = angularFlux(j-1, i, 3);
	    }
	    b += eta*finiteElement.getVyp(i)*y_upwind;
	  
	    /* x-upwind */
	    x_upwind.reset();
	    if(i == Nx-1) {
	      x_upwind = source.right(j,m);
	      b -= mu*source.right(j,m);
	    } else {
	      x_upwind[0] = angularFlux(j, i+1, 0);
	      x_upwind[1] = angularFlux(j, i+1, 1);
	      x_upwind[2] = angularFlux(j, i+1, 2);
	      x_upwind[3] = angularFlux(j, i+1, 3);
	    }
	    b -= mu*finiteElement.getVxm(j)*x_upwind;

	    /* Get Scalar Flux */
	    scalar[0] = scalarFlux1(j,i,0);
	    scalar[1] = scalarFlux1(j,i,1);
	    scalar[2] = scalarFlux1(j,i,2);
	    scalar[3] = scalarFlux1(j,i,3);

	    /* Build A and b */
	    Amatrix  = mu*finiteElement.getLx(j) + eta*finiteElement.getLy(i) + sigT*finiteElement.getM(i,j);
	    
	    /* Handle Upwinding */
	    Amatrix += (-1.0)*mu*finiteElement.getAxm(j) + eta*finiteElement.getAyp(i);

	    /* Source Vector */
	    b += (c*sigT/(2*PI))*finiteElement.getM(i,j)*scalar + source.from(i,j,m);

	    /* Solve system */
	    blaze::gesv( Amatrix, b, ipiv.get() );
	  
	    /* Update Psi */
	    angularFlux(j,i,0) = b[0];
	    angularFlux(j,i,1) = b[1];
	    angularFlux(j,i,2) = b[2];
	    angularFlux(j,i,3) = b[3];
	  
	    /* Update Phi */
	    scalarFlux2(j,i,0) += wei*b[0];
	    scalarFlux2(j,i,1) += wei*b[1];
	    scalarFlux2(j,i,2) += wei*b[2];
	    scalarFlux2(j,i,3) += wei*b[3];

	    if( i == 0 ) {
	      leak_l_1[j][m] = b[0];
	      leak_l_4[j][m] = b[3];
	    }
	    if(j == Ny-1) {
	      leak_u_3[i][m] = b[2];
	      leak_u_4[i][m] = b[3];
	    }

	  }
	}
      
      } else if(mu < 0 && eta < 0) {
	for(int j = Ny-1; j>=0; --j) {
	  for(int i = Nx-1; i>=0; --i) {
	    b.reset();
	    /* y-upwind */
	    y_upwind.reset();
	    if(j == Ny-1) {
	      y_upwind = source.top(i,m);
	      b -= eta*source.top(i,m);
	    } else {
	      y_upwind[0] = angularFlux(j+1, i, 0);
	      y_upwind[1] = angularFlux(j+1, i, 1);
	      y_upwind[2] = angularFlux(j+1, i, 2);
	      y_upwind[3] = angularFlux(j+1, i, 3);
	    }
	    b -= eta*finiteElement.getVym(i)*y_upwind;
	  
	    /* x-upwind */
	    x_upwind.reset();
	    if(i == Nx-1) {
	      x_upwind = source.right(j,m);
	      b -= mu*source.right(j,m);
	    } else {
	      x_upwind[0] = angularFlux(j, i+1, 0);
	      x_upwind[1] = angularFlux(j, i+1, 1);
	      x_upwind[2] = angularFlux(j, i+1, 2);
	      x_upwind[3] = angularFlux(j, i+1, 3);
	    }
	    b -= mu*finiteElement.getVxm(j)*x_upwind;
	    
	    /* Get Scalar Flux */
	    scalar[0] = scalarFlux1(j,i,0);
	    scalar[1] = scalarFlux1(j,i,1);
	    scalar[2] = scalarFlux1(j,i,2);
	    scalar[3] = scalarFlux1(j,i,3);

	    /* Build A and b */
	    Amatrix  = mu*finiteElement.getLx(j) + eta*finiteElement.getLy(i) + sigT*finiteElement.getM(i,j);
	    
	    /* Handle Upwinding */
	    Amatrix += (-1.0)*mu*finiteElement.getAxm(j) - eta*finiteElement.getAym(i);
	    
	    /* Source Vector */
	    b += (c*sigT/(2*PI))*finiteElement.getM(i,j)*scalar + source.from(i,j,m);

	    /* Solve system */
	    blaze::gesv( Amatrix, b, ipiv.get() );
	    
	    /* Update Psi */
	    angularFlux(j,i,0) = b[0];
	    angularFlux(j,i,1) = b[1];
	    angularFlux(j,i,2) = b[2];
	    angularFlux(j,i,3) = b[3];
	  
	    /* Update Phi */
	    scalarFlux2(j,i,0) += wei*b[0];
	    scalarFlux2(j,i,1) += wei*b[1];
	    scalarFlux2(j,i,2) += wei*b[2];
	    scalarFlux2(j,i,3) += wei*b[3];

	    if( i == 0 ) {
	      leak_l_1[j][m] = b[0];
	      leak_l_4[j][m] = b[3];
	    }

	    if(j == 0) {
	      leak_d_1[i][m] = b[0];
	      leak_d_2[i][m] = b[1];
	    }
	  }
	} 
      }
    } // End Angular Sweep

    /* Compute Error */
    double sum = 0.0;
    for(int j = 0; j < Ny; ++j) {
      for(int i = 0; i < Nx; ++i) {
    	sum += pow( scalarFlux2(j,i,0) - scalarFlux1(j,i,0) + scalarFlux2(j,i,1) - scalarFlux1(j,i,1)
    		    + scalarFlux2(j,i,2) - scalarFlux1(j,i,2) + scalarFlux2(j,i,3) - scalarFlux1(j,i,3), 2.0 );
      }
    }
    
    error2 = pow(sum/(4*Nx*Ny), 0.5);
    rho = error2/error1;
    tolerance_m = tolerance*( (1 - rho)/(rho) );
    if(error2 < tolerance_m || it >= 1e5) {
      condition = false;
    }

    for(int j = 0; j < Ny; ++j) {
      for(int i = 0; i < Nx; ++i) {
	scalarFlux1(j,i,0) = scalarFlux2(j,i,0);
	scalarFlux1(j,i,1) = scalarFlux2(j,i,1);
	scalarFlux1(j,i,2) = scalarFlux2(j,i,2);
	scalarFlux1(j,i,3) = scalarFlux2(j,i,3);
      }
    }

    error1 = error2;
    it += 1;
  } while(condition);

  /* Compute Cell Averaged Scalar Flux */
  std::vector< std::vector<double> > cellAverage(Ny, std::vector<double>(Nx, 0.0));
  for(int j = 0; j < Ny; ++j) {
    for(int i = 0; i < Nx; ++i) {
      cellAverage[j][i] = 0.25*( scalarFlux1(j,i,0) + scalarFlux1(j,i,1) + scalarFlux1(j,i,2) + scalarFlux1(j,i,3) );
    }
  }

  /* Compute Balance */
  auto xGrid = finiteElement.getXgrid();
  auto yGrid = finiteElement.getYgrid();
  double SS = 0.0;
  for(int j = 0; j < Ny; ++j) {
    for(int i = 0; i < Nx; ++i) {
      for(int m = 0; m < M; ++m) {
	double wei = std::get<2>(quadrature[m]);
	auto sor = source.from(i,j,m);
	SS += wei*(sor[0] + sor[1] + sor[2] + sor[3]);
      }
    }
  }

  double AA = 0.0;
  for(int j = 0; j < Ny; ++j) {
    double dy = yGrid[j+1] - yGrid[j];
    for(int i = 0; i < Nx; ++i) {
      double dx = xGrid[i+1] - xGrid[i];
      AA += sigT*(1 - c)*dx*dy*0.25*(scalarFlux1(j,i,0) + scalarFlux1(j,i,1) + scalarFlux1(j,i,2) + scalarFlux1(j,i,3));
    }
  }

  double curr_L = 0.0;
  double curr_R = 0.0;
  double curr_D = 0.0;
  double curr_U = 0.0;
  double in_L = 0.0;
  double in_R = 0.0;
  double in_D = 0.0;
  double in_U = 0.0;
  for(int j = 0; j < Ny; ++j) {
    double dy = yGrid[j+1] - yGrid[j];
    for(int m = 0; m < M; ++m) {
      double mui = std::get<0>(quadrature[m]);
      double wei = std::get<2>(quadrature[m]);
      curr_L += wei*( -1.0*mui )*(dy/2.0)*(leak_l_1[j][m] + leak_l_4[j][m]);
      curr_R += wei*(  1.0*mui )*(dy/2.0)*(leak_r_2[j][m] + leak_r_3[j][m]);
      auto temp_L = source.left(j,m);
      auto temp_R = source.right(j,m);
      in_L += wei*( -1.0*mui )*(temp_L[0] + temp_L[1] + temp_L[2] + temp_L[3]);
      in_R += wei*(  1.0*mui )*(temp_R[0] + temp_R[1] + temp_R[2] + temp_R[3]);
    }
  }

  for(int i = 0; i < Nx; ++i) {
    double dx = xGrid[i+1] - xGrid[i];
    for(int m = 0; m < M; ++m) {
      double eta = std::get<1>(quadrature[m]);
      double wei = std::get<2>(quadrature[m]);
      curr_D += wei*( -1.0*eta )*(dx/2.0)*(leak_d_1[i][m] + leak_d_2[i][m]);
      curr_U += wei*(  1.0*eta )*(dx/2.0)*(leak_u_3[i][m] + leak_u_4[i][m]);
      auto temp_D = source.bottom(i,m);
      auto temp_U = source.top(i,m);
      in_D += wei*( -1.0*eta )*(temp_D[0] + temp_D[1] + temp_D[2] + temp_D[3]);
      in_U += wei*(  1.0*eta )*(temp_U[0] + temp_U[1] + temp_U[2] + temp_U[3]);
    }
  }

  curr_L += in_L;
  curr_R += in_R;
  curr_D += in_D;
  curr_U += in_U;

  std::cout.precision(17);
  
  std::cout << "Number Iter:   " << it << std::endl;
  std::cout << "Final Error:   " << std::scientific << error1 << std::endl;
  std::cout << "Spec Radius:   " << std::scientific << rho << std::endl;
  std::cout << std::endl;

  std::cout << "----- Perform Particle Balance _-----" << std::endl;
  
  std::cout << "Source:        " << std::scientific << SS << std::endl;
  std::cout << "Absorption:    " << std::scientific << AA << std::endl;
  std::cout << "Current Left:  " << std::scientific << curr_L << std::endl;
  std::cout << "Current Right: " << std::scientific << curr_R << std::endl;
  std::cout << "Current Down:  " << std::scientific << curr_D << std::endl;
  std::cout << "Current Up:    " << std::scientific << curr_U << std::endl;


  double balance = SS - AA - (curr_L + curr_R + curr_U + curr_D);
  std::cout << "Balance:       " << std::scientific << balance << std::endl;
  
  std::cout << "#######################################\n";
  std::cout << std::endl;

  /* Print Flux and x/y Grids */
  std::string file1 ("xGrid.txt");
  std::string file2 ("yGrid.txt");
  std::string file3 ("flux.txt");

  std::ofstream myfile1 (file1);
  if( myfile1.is_open() ) {
    for( int i = 0; i < Nx; ++i ){
      myfile1 << std::setprecision(15) << 0.5*(xGrid[i] + xGrid[i+1])  << "\n";
    }
  }
  myfile1.close();

  std::ofstream myfile2 (file2);
  if( myfile2.is_open() ) {
    for( int j = 0; j < Ny; ++j ){
      myfile2 << std::setprecision(15) << 0.5*(yGrid[j] + yGrid[j+1])  << "\n";
    }
  }
  myfile2.close();
  
  std::ofstream myfile3 (file3);
  if( myfile3.is_open() ) {
    for( int j = 0; j < Ny; ++j ){
      for( int i = 0; i < Nx; ++i ){
	myfile3 << std::setprecision(15) << cellAverage[j][i] << " ";
      }
      myfile3 << std::endl;
    }
  }
  myfile3.close();
  
  return cellAverage;
}
