template<typename S, typename L, typename R, typename D, typename U>
struct Problem {
  S source;
  L lSource;
  R rSource;
  D dSource;
  U uSource;

  double xMin, xMax, yMin, yMax;
  int Nx, Ny;
  double sigT, c;

  Problem(S& source, L& lSource, R& rSource, D& dSource, U& uSource,
	  double xMin, double xMax, double yMin, double yMax, int Nx, int Ny, double sigT, double c)
    : source(source), lSource(lSource), rSource(rSource), dSource(dSource), uSource(uSource),
      xMin(xMin), xMax(xMax), yMin(yMin), yMax(yMax), Nx(Nx), Ny(Ny), sigT(sigT), c(c) {}
};
