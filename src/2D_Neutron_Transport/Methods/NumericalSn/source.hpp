template<typename Q, typename P>
class Source {
  Q quad;
  P problem;
  int Nx, Ny, M;
  std::unique_ptr<double[]> N1, N2, N3, N4;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source1;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source2;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source3;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source4;

  std::unique_ptr<double[]> L1, L4, R2, R3, D1, D2, U3, U4;

  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_l_1;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_l_4;

  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_r_2;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_r_3;

  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_d_1;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_d_2;

  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_u_3;
  stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>, stdex::layout_right> source_u_4;

  
public:
  Source(Q& quad, P& problem) : quad(quad), problem(problem)
  {
    this->M  = quad.getQuadrature().size();
    this->Nx = problem.Nx;
    this->Ny = problem.Ny;
    this->N1 = std::make_unique<double[]>(this->Ny * this->Nx * this->M);
    this->N2 = std::make_unique<double[]>(this->Ny * this->Nx * this->M);
    this->N3 = std::make_unique<double[]>(this->Ny * this->Nx * this->M);
    this->N4 = std::make_unique<double[]>(this->Ny * this->Nx * this->M);

    this->source1 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>,
					stdex::layout_right>(this->N1.get(), this->Ny, this->Nx, this->M);
    this->source2 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>,
					stdex::layout_right>(this->N2.get(), this->Ny, this->Nx, this->M);
    this->source3 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>,
					stdex::layout_right>(this->N3.get(), this->Ny, this->Nx, this->M);
    this->source4 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent, stdex::dynamic_extent>,
					stdex::layout_right>(this->N4.get(), this->Ny, this->Nx, this->M);

    this->L1 = std::make_unique<double[]>(this->Ny * this->M);
    this->L4 = std::make_unique<double[]>(this->Ny * this->M);

    this->R2 = std::make_unique<double[]>(this->Ny * this->M);
    this->R3 = std::make_unique<double[]>(this->Ny * this->M);

    this->D1 = std::make_unique<double[]>(this->Nx * this->M);
    this->D2 = std::make_unique<double[]>(this->Nx * this->M);

    this->U3 = std::make_unique<double[]>(this->Nx * this->M);
    this->U4 = std::make_unique<double[]>(this->Nx * this->M);

    this->source_l_1 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->L1.get(), this->Ny, this->M);
    this->source_l_4 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->L4.get(), this->Ny, this->M);

    this->source_r_2 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->R2.get(), this->Ny, this->M);
    this->source_r_3 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->R3.get(), this->Ny, this->M);
    
    this->source_d_1 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->D1.get(), this->Nx, this->M);
    this->source_d_2 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->D2.get(), this->Nx, this->M);
    
    this->source_u_3 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->U3.get(), this->Nx, this->M);
    this->source_u_4 = stdex::basic_mdspan<double, stdex::extents<stdex::dynamic_extent, stdex::dynamic_extent>,
					   stdex::layout_right>(this->U4.get(), this->Nx, this->M);
  }

  template<typename E, typename R>
  void calcSource(E&& finiteElement, R&& quad_2d)
  {
    auto quadrature = this->quad.getQuadrature();
    auto xGrid = finiteElement.getXgrid();
    auto yGrid = finiteElement.getYgrid();

    auto B1 = [](auto&& x, auto&& y, auto&& xL, auto&& xR, auto&& yD, auto&& yU){ return (1.0/( (xR - xL)*(yU - yD) ))*(xR - x)*(yU - y); };
    auto B2 = [](auto&& x, auto&& y, auto&& xL, auto&& xR, auto&& yD, auto&& yU){ return (1.0/( (xR - xL)*(yU - yD) ))*(x - xL)*(yU - y); };
    auto B3 = [](auto&& x, auto&& y, auto&& xL, auto&& xR, auto&& yD, auto&& yU){ return (1.0/( (xR - xL)*(yU - yD) ))*(x - xL)*(y - yD); };
    auto B4 = [](auto&& x, auto&& y, auto&& xL, auto&& xR, auto&& yD, auto&& yU){ return (1.0/( (xR - xL)*(yU - yD) ))*(xR - x)*(y - yD); };

    /* Cell Center Source */
    for(int m = 0; m < this->M; ++m) {
      double mu  = std::get<0>(quadrature[m]);
      double eta = std::get<1>(quadrature[m]);
      for(int i = 0; i < this->Nx; ++i) {
    	double xL = xGrid[i+0];
    	double xR = xGrid[i+1];
    	for(int j = 0; j < this->Ny; ++j) {
    	  double yD = yGrid[j+0];
    	  double yU = yGrid[j+1];

	  auto f1 = [&](auto&& x, auto&& y){ return B1(x,y,xL,xR,yD,yU)*this->problem.source(x,y,mu,eta); };
	  auto f2 = [&](auto&& x, auto&& y){ return B2(x,y,xL,xR,yD,yU)*this->problem.source(x,y,mu,eta); };
	  auto f3 = [&](auto&& x, auto&& y){ return B3(x,y,xL,xR,yD,yU)*this->problem.source(x,y,mu,eta); };
	  auto f4 = [&](auto&& x, auto&& y){ return B4(x,y,xL,xR,yD,yU)*this->problem.source(x,y,mu,eta); };

	  this->source1(j,i,m) = quad_2d.integrate(f1, xL, xR, yD, yU);
	  this->source2(j,i,m) = quad_2d.integrate(f2, xL, xR, yD, yU);
	  this->source3(j,i,m) = quad_2d.integrate(f3, xL, xR, yD, yU);
	  this->source4(j,i,m) = quad_2d.integrate(f4, xL, xR, yD, yU);

    	}
      }
    }

    /* Boundary Sources */
    for(int m = 0; m < this->M; ++m) {
      double mu  = std::get<0>(quadrature[m]);
      double eta = std::get<1>(quadrature[m]);
      /* Left Boundary */
      if( mu > 0 ) {
	double xL = xGrid[0];
	double xR = xGrid[1];
    	for(int j = 0; j < this->Ny; ++j) {
    	  double yD = yGrid[j+0];
    	  double yU = yGrid[j+1];
	  auto l1 = [&](auto&& y){ return B1(this->problem.xMin,y,xL,xR,yD,yU)*this->problem.lSource(y,mu,eta); };
	  auto l4 = [&](auto&& y){ return B4(this->problem.xMin,y,xL,xR,yD,yU)*this->problem.lSource(y,mu,eta); };
	  this->source_l_1(j,m) = quad_2d.integrate_1D(l1,yD,yU);
    	  this->source_l_4(j,m) = quad_2d.integrate_1D(l4,yD,yU);
    	}
      }
      /* Right Boundary */
      if( mu < 0 ) {
	double xL = xGrid[this->Nx-1];
	double xR = xGrid[this->Nx-0];
    	for(int j = 0; j < this->Ny; ++j) {
    	  double yD = yGrid[j+0];
    	  double yU = yGrid[j+1];
	  auto r2 = [&](auto&& y){ return B2(this->problem.xMax,y,xL,xR,yD,yU)*this->problem.rSource(y,mu,eta); };
	  auto r3 = [&](auto&& y){ return B3(this->problem.xMax,y,xL,xR,yD,yU)*this->problem.rSource(y,mu,eta); };
	  this->source_r_2(j,m) = quad_2d.integrate_1D(r2,yD,yU);
    	  this->source_r_3(j,m) = quad_2d.integrate_1D(r3,yD,yU);
    	}
      }
      /* Bottom Boundary */
      if( eta > 0 ) {
	double yD = yGrid[0];
	double yU = yGrid[1];
    	for(int i = 0; i < this->Nx; ++i) {
    	  double xL = xGrid[i+0];
    	  double xR = xGrid[i+1];
	  auto d1 = [&](auto&& x){ return B1(x,this->problem.yMin,xL,xR,yD,yU)*this->problem.dSource(x,mu,eta); };
	  auto d2 = [&](auto&& x){ return B2(x,this->problem.yMin,xL,xR,yD,yU)*this->problem.dSource(x,mu,eta); };
    	  this->source_d_1(i,m) = quad_2d.integrate_1D(d1,xL,xR);
    	  this->source_d_2(i,m) = quad_2d.integrate_1D(d2,xL,xR);
    	}
      }
      /* Top Boundary */
      if( eta < 0 ) {
	double yD = yGrid[this->Ny-1];
	double yU = yGrid[this->Ny-0];
    	for(int i = 0; i < this->Nx; ++i) {
    	  double xL = xGrid[i+0];
    	  double xR = xGrid[i+1];
	  auto u3 = [&](auto&& x){ return B3(x,this->problem.yMax,xL,xR,yD,yU)*this->problem.uSource(x,mu,eta); };
	  auto u4 = [&](auto&& x){ return B4(x,this->problem.yMax,xL,xR,yD,yU)*this->problem.uSource(x,mu,eta); };
    	  this->source_u_3(i,m) = quad_2d.integrate_1D(u3,xL,xR);
    	  this->source_u_4(i,m) = quad_2d.integrate_1D(u4,xL,xR);
    	}
      }
    }
  }

  blaze::DynamicVector<double, blaze::columnVector> from(int i, int j, int m)
  {
    blaze::DynamicVector<double, blaze::columnVector> vec(4, 0.0);
    vec[0] = this->source1(j, i, m);
    vec[1] = this->source2(j, i, m);
    vec[2] = this->source3(j, i, m);
    vec[3] = this->source4(j, i, m);
    
    return vec;
  }

  blaze::DynamicVector<double, blaze::columnVector> left(int j, int m)
  {
    blaze::DynamicVector<double, blaze::columnVector> vec(4, 0.0);
    vec[0] = this->source_l_1(j,m);
    vec[1] = 0.0;
    vec[2] = 0.0;
    vec[3] = this->source_l_4(j,m);

    return vec;
  }

  blaze::DynamicVector<double, blaze::columnVector> right(int j, int m)
  {
    blaze::DynamicVector<double, blaze::columnVector> vec(4, 0.0);
    vec[0] = 0.0;
    vec[1] = this->source_r_2(j,m);
    vec[2] = this->source_r_3(j,m);
    vec[3] = 0.0;

    return vec;
  }

  blaze::DynamicVector<double, blaze::columnVector> bottom(int i, int m)
  {
    blaze::DynamicVector<double, blaze::columnVector> vec(4, 0.0);
    vec[0] = this->source_d_1(i,m);
    vec[1] = this->source_d_2(i,m);
    vec[2] = 0.0;
    vec[3] = 0.0;

    return vec;
  }

  blaze::DynamicVector<double, blaze::columnVector> top(int i, int m)
  {
    blaze::DynamicVector<double, blaze::columnVector> vec(4, 0.0);
    vec[0] = 0.0;
    vec[1] = 0.0;
    vec[2] = this->source_u_3(i,m);
    vec[3] = this->source_u_4(i,m);

    return vec;
  }
  
};
