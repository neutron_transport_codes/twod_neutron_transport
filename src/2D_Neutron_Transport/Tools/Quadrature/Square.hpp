template<int N>
class Square {
private:
  enum {ORDER = N};
  std::vector<std::tuple<double, double, double>> quadrature;
  
  std::vector<double> legNodes;
  std::vector<double> legWeights;

  double polynomial(int n, double x){
    if( n <  0 ){
      return 0;
    } else if ( n == 0 ){
      return 1;
    } else if ( n == 1 ){
      return x;
    } else {
      double P; 
      double Pnn = 1;
      double Pn  = x;
      for(int k = 2; k <= n; ++k){
	P = ((2*static_cast<double>(k) - 1)*x*Pn - (static_cast<double>(k) - 1)*Pnn)/static_cast<double>(k);
	Pnn = Pn;
	Pn  = P;
      }
      return P;
    }
  }

  double dPolynomial(int n, double x){
    double Pn  = this->polynomial(n,   x);
    double Pnn = this->polynomial(n-1, x);
    return static_cast<double>(n)/(x*x - 1)*(x*Pn - Pnn);
  }

  void genLegendreNodes(){
    double eps = std::numeric_limits<double>::epsilon();
    for(int i = 0; i < this->ORDER; ++i){
      double xm = cos(PI*(i+1 - 0.25)/(this->ORDER + 0.5));
      double xn = 0;
      bool cond = true;
      int it = 0;
      do {
	xn = xm - this->polynomial(this->ORDER, xm)/this->dPolynomial(this->ORDER, xm);
	double err = abs((xn - xm)/xn);
	if( err <= eps || it >= 1e2 ){
	  cond = false;
	}
	xm = xn;
	++it;
      } while(cond);
      this->legNodes.push_back(xn);
    }
  }

  void genLegendreWeights(){
    for(int i = 0; i < this->ORDER; ++i){
      double wi = 2/( (1 - this->legNodes[i]*this->legNodes[i])*(pow(this->dPolynomial(this->ORDER, this->legNodes[i]), 2)) );
      this->legWeights.push_back(wi);
    }
  }

  void genQuadrature(){
    for(int i = 0; i < static_cast<int>(this->ORDER/2); ++i){
      double v_j = PI/static_cast<double>(this->ORDER);
      double x_i  = this->legNodes[i];

      for(int j = 0; j < static_cast<int>(this->ORDER/2); ++ j){
	double p_j  = (PI/2.0)*(2.0*static_cast<double>(j+1) - 1)/(static_cast<double>(this->ORDER));
	double n_ij = pow( 1 - x_i*x_i, 0.5)*cos(p_j);
	std::tuple<double, double, double> point1 = std::make_tuple(     x_i,      n_ij, this->legWeights[i]*v_j);
	std::tuple<double, double, double> point2 = std::make_tuple((-1)*x_i,      n_ij, this->legWeights[i]*v_j);
	std::tuple<double, double, double> point3 = std::make_tuple(     x_i, (-1)*n_ij, this->legWeights[i]*v_j);
	std::tuple<double, double, double> point4 = std::make_tuple((-1)*x_i, (-1)*n_ij, this->legWeights[i]*v_j);
	this->quadrature.push_back(point1);
	this->quadrature.push_back(point2);
	this->quadrature.push_back(point3);
	this->quadrature.push_back(point4);
      }      
    }
  }

public:
  Square() {
    this->genLegendreNodes();
    this->genLegendreWeights();
    this->genQuadrature();
  }

  std::vector<std::tuple<double, double, double>> getQuadrature(){
    return this->quadrature;
  }

  template<typename F>
  double integrate(F&& func){
    int R = this->quadrature.size();
    double sum = 0.0;
    for(int i = 0; i < R; ++i){
      double mu_i  = std::get<0>(this->quadrature[i]);
      double eta_i = std::get<1>(this->quadrature[i]);
      double weigh = std::get<2>(this->quadrature[i]);
      sum += weigh*func( mu_i, eta_i);
    }
    return sum;
  }

};
