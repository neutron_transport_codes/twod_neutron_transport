template<int N, int M>
class TwoDimensionalLegendre {
  enum {xORDER = N};
  enum {yORDER = M};
  std::vector<double> x_nodes;
  std::vector<double> x_weights;
  std::vector<double> y_nodes;
  std::vector<double> y_weights;
  

  double polynomial(int n, double x){
    if( n <  0 ){
      return 0;
    } else if ( n == 0 ){
      return 1;
    } else if ( n == 1 ){
      return x;
    } else {
      double P; 
      double Pnn = 1;
      double Pn  = x;
      for(int k = 2; k <= n; ++k){
	P = ((2*static_cast<double>(k) - 1)*x*Pn - (static_cast<double>(k) - 1)*Pnn)/static_cast<double>(k);
	Pnn = Pn;
	Pn  = P;
      }
      return P;
    }
  }

  double dPolynomial(int n, double x){
    double Pn  = this->polynomial(n,   x);
    double Pnn = this->polynomial(n-1, x);
    return static_cast<double>(n)/(x*x - 1)*(x*Pn - Pnn);
  }

  void genXNodes(){
    double eps = std::numeric_limits<double>::epsilon();
    for(int i = 0; i < this->xORDER; ++i){
      double xm = cos(PI*(i+1 - 0.25)/(this->xORDER + 0.5));
      double xn = 0;
      bool cond = true;
      int it = 0;
      do {
	xn = xm - this->polynomial(this->xORDER, xm)/this->dPolynomial(this->xORDER, xm);
	double err = abs((xn - xm)/xn);
	if( err <= eps || it >= 1e2 ){
	  cond = false;
	}
	xm = xn;
	++it;
      } while(cond);
      this->x_nodes.push_back(xn);
    }
  }

  void genXWeights(){
    for(int i = 0; i < this->xORDER; ++i){
      double wi = 2/( (1 - this->x_nodes[i]*this->x_nodes[i])*(pow(this->dPolynomial(this->xORDER, this->x_nodes[i]), 2)) );
      this->x_weights.push_back(wi);
    }
  }

  void genYNodes(){
    double eps = std::numeric_limits<double>::epsilon();
    for(int i = 0; i < this->yORDER; ++i){
      double xm = cos(PI*(i+1 - 0.25)/(this->yORDER + 0.5));
      double xn = 0;
      bool cond = true;
      int it = 0;
      do {
	xn = xm - this->polynomial(this->yORDER, xm)/this->dPolynomial(this->yORDER, xm);
	double err = abs((xn - xm)/xn);
	if( err <= eps || it >= 1e2 ){
	  cond = false;
	}
	xm = xn;
	++it;
      } while(cond);
      this->y_nodes.push_back(xn);
    }
  }

  void genYWeights(){
    for(int i = 0; i < this->yORDER; ++i){
      double wi = 2/( (1 - this->y_nodes[i]*this->y_nodes[i])*(pow(this->dPolynomial(this->yORDER, this->y_nodes[i]), 2)) );
      this->y_weights.push_back(wi);
    }
  }

public:
  TwoDimensionalLegendre() {
    this->genXNodes();
    this->genXWeights();
    this->genYNodes();
    this->genYWeights();
  }

  auto getXNodes(){
    return this->x_nodes;
  }

  auto getWeights(){
    return this->x_weights;
  }

  template<typename F>
  double integrate_1D(F&& func, double a, double b) {
    double sum = 0.0;
    for(int i = 0; i < this->xORDER; ++i) {
      double xi = ((b - a)/2.0)*this->x_nodes[i] + (b + a)/2.0;
      double wi = ((b - a)/2.0)*this->x_weights[i];
      sum += wi*func(xi);
    }
    return sum;
  }
  
  template<typename F>
  double integrate(F&& func, double a, double b, double c, double d){
    double sum = 0;
    for(int i = 0; i < this->xORDER; ++i) {
      double xi = ((b - a)/2.0)*this->x_nodes[i] + (b + a)/2.0;
      double wi = ((b - a)/2.0)*this->x_weights[i];
      for(int j = 0; j < this->yORDER; ++j) {
    	double yj = ((d - c)/2.0)*this->y_nodes[j] + (d + c)/2.0;
    	double wj = ((d - c)/2.0)*this->y_weights[j];
    	sum += wi*wj*func(xi, yj);
      }
    }
    return sum;
  }

};
