#define CATCH_CONFIG_MAIN
#include "2D_Neutron_Transport.hpp"
#include <catch2/catch.hpp>

using namespace NTR_2D::Tools;


SCENARIO("Test Quadrature::Triangular") {

  auto func1 = [](double mu, double eta){ return 0.0*mu + 0.0*eta + 1.0; };
  auto func2 = [](double mu, double eta){ return mu*mu + 0.0*eta;  };
  auto func3 = [](double mu, double eta){ return 0.0*mu + eta*eta; };
  auto func4 = [](double mu, double eta){ return (mu*mu)*(eta*eta); };
  auto func5 = [](double mu, double eta){ return (mu*mu*mu*mu)*(eta*eta); };
  auto func6 = [](double mu, double eta){ return (mu*mu*mu*mu*mu*mu)*(eta*eta); };
  
  WHEN(" using a second order quadrature, ") {
    Quad::Triangular<2> quadT2;
    double result1 = quadT2.integrate(func1);
    double result2 = quadT2.integrate(func2);
    double result3 = quadT2.integrate(func3);

    REQUIRE(result1 == Approx(2*PI/1.0 ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0 ).epsilon(1e-14) );
  }

  WHEN(" using a fourth order quadrature, ") {
    Quad::Triangular<4> quadT4;
    double result1 = quadT4.integrate(func1);
    double result2 = quadT4.integrate(func2);
    double result3 = quadT4.integrate(func3);
    double result4 = quadT4.integrate(func4);

    REQUIRE(result1 == Approx(2*PI/1.0 ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(2*PI/15.0).epsilon(1e-14) );
  }

  WHEN(" using a sixth order quadrature, ") {
    Quad::Triangular<6> quadT6;
    double result1 = quadT6.integrate(func1);
    double result2 = quadT6.integrate(func2);
    double result3 = quadT6.integrate(func3);
    double result4 = quadT6.integrate(func4);
    double result5 = quadT6.integrate(func5);

    REQUIRE(result1 == Approx(2*PI/1.0 ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(2*PI/15.0).epsilon(1e-14) );
    REQUIRE(result5 == Approx(2*PI/35.0).epsilon(1e-14) );
  }

  WHEN(" using an eighth order quadrature, ") {
    Quad::Triangular<8> quadT8;
    double result1 = quadT8.integrate(func1);
    double result2 = quadT8.integrate(func2);
    double result3 = quadT8.integrate(func3);
    double result4 = quadT8.integrate(func4);
    double result5 = quadT8.integrate(func5);
    double result6 = quadT8.integrate(func6);

    REQUIRE(result1 == Approx(2*PI/1.0 ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(2*PI/15.0).epsilon(1e-14) );
    REQUIRE(result5 == Approx(2*PI/35.0).epsilon(1e-14) );
    REQUIRE(result6 == Approx(2*PI/63.0).epsilon(1e-14) );
  }
  
}

SCENARIO("Test Quadrature::Square") {

  auto func1 = [](double mu, double eta){ return 0.0*mu + 0.0*eta + 1.0; };
  auto func2 = [](double mu, double eta){ return mu*mu + 0.0*eta;  };
  auto func3 = [](double mu, double eta){ return 0.0*mu + eta*eta; };
  auto func4 = [](double mu, double eta){ return (mu*mu)*(eta*eta); };
  auto func5 = [](double mu, double eta){ return (mu*mu*mu*mu)*(eta*eta*eta*eta); };
  auto func6 = [](double mu, double eta){ return (mu*mu*mu*mu*mu*mu)*(eta*eta*eta*eta*eta*eta); };
  
  WHEN(" using a second order quadrature, ") {
    Quad::Square<2> quadS2;
    double result1 = quadS2.integrate(func1);
    double result2 = quadS2.integrate(func2);
    double result3 = quadS2.integrate(func3);

    REQUIRE(result1 == Approx(2*PI/1.0 ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0 ).epsilon(1e-14) );
  }

  WHEN(" using a fourth order quadrature, ") {
    Quad::Square<4> quadS4;
    double result1 = quadS4.integrate(func1);
    double result2 = quadS4.integrate(func2);
    double result3 = quadS4.integrate(func3);
    double result4 = quadS4.integrate(func4);

    REQUIRE(result1 == Approx(2*PI/1.0 ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0 ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(2*PI/15.0).epsilon(1e-14) );
  }

  WHEN(" using a sixth order quadrature, ") {
    Quad::Square<6> quadS6;
    double result1 = quadS6.integrate(func1);
    double result2 = quadS6.integrate(func2);
    double result3 = quadS6.integrate(func3);
    double result4 = quadS6.integrate(func4);
    double result5 = quadS6.integrate(func5);

    REQUIRE(result1 == Approx(2*PI/1.0  ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0  ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0  ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(2*PI/15.0 ).epsilon(1e-14) );
    REQUIRE(result5 == Approx(2*PI/105.0).epsilon(1e-14) );
  }

  WHEN(" using an eighth order quadrature, ") {
    Quad::Square<8> quadS8;
    double result1 = quadS8.integrate(func1);
    double result2 = quadS8.integrate(func2);
    double result3 = quadS8.integrate(func3);
    double result4 = quadS8.integrate(func4);
    double result5 = quadS8.integrate(func5);
    double result6 = quadS8.integrate(func6);

    REQUIRE(result1 == Approx(2*PI/1.0    ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(2*PI/3.0    ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(2*PI/3.0    ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(2*PI/15.0   ).epsilon(1e-14) );
    REQUIRE(result5 == Approx(2*PI/105.0  ).epsilon(1e-14) );
    REQUIRE(result6 == Approx(10*PI/3003.0).epsilon(1e-14) );
  }

}
