#include "2D_Neutron_Transport.hpp"
#include <catch2/catch.hpp>

using namespace NTR_2D::Tools;

SCENARIO("Test Quadrature::TwoDimensionalLegendre on 2D integration") {
  auto func1 = [](double x, double y) { return 0.0*x + 0.0*y + 1.0; };
  auto func2 = [](double x, double y) { return pow(x, 2.0)*pow(y, 2.0); };
  auto func3 = [](double x, double y) { return pow(x, 4.0)*pow(y, 4.0); };
  auto func4 = [](double x, double y) { return pow(x, 8.0)*pow(y, 8.0); };

  WHEN(" using a second order quadrature, ") {
    Quad::TwoDimensionalLegendre<2, 2> quad_2d;
    double result1 = quad_2d.integrate(func1, 1.0, 2.0, 1.0, 2.0);
    double result2 = quad_2d.integrate(func2, 1.0, 2.0, 1.0, 2.0);

    REQUIRE(result1 == Approx(1.0       ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(49.0/9.0  ).epsilon(1e-14) );
  }

  WHEN(" using a fourth order quadrature, ") {
    Quad::TwoDimensionalLegendre<4, 4> quad_4d;
    double result1 = quad_4d.integrate(func1, 1.0, 2.0, 1.0, 2.0);
    double result2 = quad_4d.integrate(func2, 1.0, 2.0, 1.0, 2.0);
    double result3 = quad_4d.integrate(func3, 1.0, 2.0, 1.0, 2.0);

    REQUIRE(result1 == Approx(1.0          ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(49.0/9.0     ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(961.0/25.0   ).epsilon(1e-14) );
  }

  WHEN(" using a sixth order quadrature, ") {
    Quad::TwoDimensionalLegendre<6, 6> quad_4d;
    double result1 = quad_4d.integrate(func1, 1.0, 2.0, 1.0, 2.0);
    double result2 = quad_4d.integrate(func2, 1.0, 2.0, 1.0, 2.0);
    double result3 = quad_4d.integrate(func3, 1.0, 2.0, 1.0, 2.0);
    double result4 = quad_4d.integrate(func4, 1.0, 2.0, 1.0, 2.0);

    REQUIRE(result1 == Approx(1.0          ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(49.0/9.0     ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(961.0/25.0   ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(261121.0/81.0).epsilon(1e-14) );
  }  
}

SCENARIO("Test Quadrature::TwoDimensionalLegendre on 1D integration") {
  auto func1 = [](double x) { return 0.0*x + 1.0; };
  auto func2 = [](double x) { return pow(x, 2.0); };
  auto func3 = [](double x) { return pow(x, 4.0); };
  auto func4 = [](double x) { return pow(x, 8.0); };

  WHEN(" using a second order quadrature, ") {
    Quad::TwoDimensionalLegendre<2, 2> quad_2d;

    double result1 = quad_2d.integrate_1D(func1, 1.0, 2.0);
    double result2 = quad_2d.integrate_1D(func2, 1.0, 2.0);

    REQUIRE(result1 == Approx(1.0     ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(7.0/3.0 ).epsilon(1e-14) );
  }

  WHEN(" using a fourth order quadrature, ") {
    Quad::TwoDimensionalLegendre<4, 4> quad_4d;
    double result1 = quad_4d.integrate_1D(func1, 1.0, 2.0);
    double result2 = quad_4d.integrate_1D(func2, 1.0, 2.0);
    double result3 = quad_4d.integrate_1D(func3, 1.0, 2.0);

    REQUIRE(result1 == Approx(1.0      ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(7.0/3.0  ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(31.0/5.0 ).epsilon(1e-14) );
  }

  WHEN(" using a sixth order quadrature, ") {
    Quad::TwoDimensionalLegendre<6, 6> quad_4d;
    double result1 = quad_4d.integrate_1D(func1, 1.0, 2.0);
    double result2 = quad_4d.integrate_1D(func2, 1.0, 2.0);
    double result3 = quad_4d.integrate_1D(func3, 1.0, 2.0);
    double result4 = quad_4d.integrate_1D(func4, 1.0, 2.0);

    REQUIRE(result1 == Approx(1.0       ).epsilon(1e-14) );
    REQUIRE(result2 == Approx(7.0/3.0   ).epsilon(1e-14) );
    REQUIRE(result3 == Approx(31.0/5.0  ).epsilon(1e-14) );
    REQUIRE(result4 == Approx(511.0/9.0 ).epsilon(1e-14) );
  }

}
