#ifndef TD_NTR_HPP
#define TD_NTR_HPP

#include <iostream>
#include <vector>
#include <math.h> 
#include <tuple>
#include <string>
#include <fstream>

#include <blaze/Math.h>
#include <experimental/mdspan>

#define PI 3.14159265358979324

namespace NTR_2D {

  namespace stdex = std::experimental;
#include "2D_Neutron_Transport/Tools.hpp"
#include "2D_Neutron_Transport/Methods.hpp"
  
}

#endif
