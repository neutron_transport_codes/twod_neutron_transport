#include "2D_Neutron_Transport.hpp"

int main() {
  double XMIN = 0.0;
  double XMAX = 2.0;
  int Nx = 16;

  double YMIN = 0.0;
  double YMAX = 4.0;
  int Ny = 40;
  
  double sigT = 2.0;
  double c    = 0.99;
 
  double TOLERANCE = 1e-8;

  // auto Q = [](auto&& x, auto&& y, auto&& mu, auto&& eta) { return 0.0*x + 0.0*y + 0.0*mu + 0.0*eta + 1.0/(2.0*PI); };
  auto Q = [sigT, c](auto&& x, auto&& y, auto&& mu, auto&& eta) {
	     return mu*y + eta*x + sigT*(1 - c)*x*y; };
  /* Left Boundary */
  auto f = [](auto&& y, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + 0.0*y; };
  /* Right Boundary */
  auto g = [XMAX](auto&& y, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + XMAX*y; };
  /* Down Boundary */
  auto h = [](auto&& x, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + 0.0*x; };
  /* Up  Boundary */
  auto i = [YMAX](auto&& x, auto&& mu, auto&& eta) { return 0.0*mu + 0.0*eta + YMAX*x; };
  
  NTR_2D::Methods::NumericalSn::Problem<decltype(Q), decltype(f), decltype(g), decltype(h), decltype(i)>
    prob(Q, f, g, h, i, XMIN, XMAX, YMIN, YMAX, Nx, Ny, sigT, c);
  
  NTR_2D::Tools::Quad::Square<8> quad;
  auto quadrature = quad.getQuadrature();

  NTR_2D::Tools::Quad::TwoDimensionalLegendre<2, 2> quad_2d;
  
  NTR_2D::Methods::NumericalSn::FiniteElement<decltype(prob)>          finiteElement(prob);
  NTR_2D::Methods::NumericalSn::Source<decltype(quad), decltype(prob)> source(quad, prob);
  source.calcSource(finiteElement, quad_2d);

  auto scalar = NTR_2D::Methods::NumericalSn::solver(prob, quad, source, finiteElement, TOLERANCE);

  auto xGrid = finiteElement.getXgrid();
  auto yGrid = finiteElement.getYgrid();

  std::vector< std::vector<double> > mms(Ny, std::vector<double>(Nx, 0.0) );
  auto func = [XMAX, YMAX](auto&& x, auto && y){ return 2*PI*x*y; };
  for(int j = 0; j < Ny; ++j) {
    double dy = yGrid[j+1] - yGrid[j];
    for(int i = 0; i < Nx; ++i) {
      double dx = xGrid[i+1] - xGrid[i];
      mms[j][i] = (1.0/(dy*dx))*quad_2d.integrate(func, xGrid[i], xGrid[i+1], yGrid[j], yGrid[j+1]);
    }
  }

  // Compute Error //
  double sum = 0.0;
  for(int j = 0; j < Ny; ++j) {
    for(int i = 0; i < Nx; ++i) {
      sum += pow( mms[j][i] - scalar[j][i], 2.0 );
    }
  }

  double error = pow( 1.0/(Nx*Ny)*sum, 0.5 );

  std::cout << "Error: " << error << std::endl;
  
}
